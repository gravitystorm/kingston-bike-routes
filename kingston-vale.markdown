---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Kingston to Kingston Vale

## Eastbound

### Part 1 - Wheatfield Way to Station Road

### Part 2 - Station Road to Crescent Road

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/537444115?h=0b510b4fd6&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="C30 - Kingston to Kingston Vale (section)"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

### Part 3 - Crescent Road onwards

## Westbound

### Part 1 - Kingston Vale to Crescent Road

### Part 2 - Crescent Road to Station Road

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/537656382?h=959b63b8cd&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="C30 Kingston Vale to Kingston (section)"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

### Part 3 - Station Road to Wheatfield Way
